import pygame
import PyParticles

pygame.display.set_caption('Ballz')
(width, height) = (1820, 980)
screen = pygame.display.set_mode((width, height))
#screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN, pygame.RESIZABLE)
env = PyParticles.Environment(width, height)
clock = pygame.time.Clock()

#env.addParticles(50)
env.addDestroyParticles(50)
#env.addParticles(x=200, y=250, size=10, speed=4, angle=0)
#env.addParticles(x=200, y=300, size=10, speed=3, angle=90)
#env.particles[0].colour = (255,0,0)

selected_particle = None
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()
            selected_particle = env.findParticle(pos[0], pos[1])
        elif event.type == pygame.MOUSEBUTTONUP:
            selected_particle = None
        """
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                env.addParticles()
        """
    keys=pygame.key.get_pressed()
    if keys[pygame.K_SPACE]:
        env.addParticles()

    if selected_particle:
        pos = pygame.mouse.get_pos()
        selected_particle.mouseMove(pos[0], pos[1])

    env.update()
    screen.fill(env.colour)

    #print "Speed p1: " + str(env.particles[0].speed)
    """
    ret = 0
    for s in env.particles:
        ret+=s.speed
    #print "Cum speed: " + str(ret)
    print "Mean speed: " + str(ret/len(env.particles))
    """
    for p in env.particles:
        pygame.draw.circle(screen, p.colour, (int(p.x), int(p.y)), p.size, p.thickness)

    pygame.display.flip()
    clock.tick(60)
